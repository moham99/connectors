from social_interaction_cloud.basic_connector import BasicSICConnector
from social_interaction_cloud.action import ActionRunner


class WorldInfo:
    def __init__(self, robot: ActionRunner, sic: BasicSICConnector, known_persons: set = None, ids_to_name: dict = None) -> None:
        self.__robot = robot
        self.__sic = sic
        self.__known_persons = known_persons if known_persons else set()
        self.__id_to_names = ids_to_name if ids_to_name else dict()

    def get_robot(self) -> ActionRunner:
        return self.__robot

    def get_sic(self) -> BasicSICConnector:
        return self.__sic

    def get_known_persons(self) -> set:
        return self.__known_persons

    def person_is_known(self, person: str) -> bool:
        return person in self.__known_persons

    def add_known_person(self, person: str) -> None:
        self.__known_persons.add(person)

    def get_person_name(self, identifier: str) -> str:
        return self.__id_to_names.get(identifier)
