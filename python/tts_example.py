from time import sleep
from social_interaction_cloud.basic_connector import BasicSICConnector
from social_interaction_cloud.action import ActionRunner



class TTSConnector(BasicSICConnector):
    """This example shows you how to create your own Text-To-Speech SIC connector by inheriting BasicSICConnector.
    For more information go to
    https://socialrobotics.atlassian.net/wiki/spaces/CBSR/pages/616398873/Python+connector#Basic-SIC-Connector    
    """

    def __init__(self, server_ip: str, tts_key: str, tts_voice: str):
        super(TTSConnector, self).__init__(server_ip, tts_key_file = tts_key, tts_voice = tts_voice)


class Example:
    """ Example of playing text via the robot's speakers. The service use Google's Text-To-Speech service. The languages and voices have thus a much wider range
    than just the robot's in-built voices.

    The voice can be selected from https://cloud.google.com/text-to-speech/docs/voices. The language is set automatically based on the voice.
    """
    def __init__(self, server_ip: str, tts_key_file: str = None, tts_voice: str = None):
        self.sic = TTSConnector(server_ip, tts_key_file, tts_voice)
        self.action_runner = ActionRunner(self.sic)


    def run(self) -> None:
        # Start the Social Interaction Cloud
        self.sic.start()

        self.action_runner.load_waiting_action('wake_up')
        self.action_runner.load_waiting_action('say_text_to_speech', 'Hello, world, how are you?')
        self.action_runner.run_loaded_actions()

        self.action_runner.run_waiting_action('rest')
        self.sic.stop()


example = Example('127.0.0.1', 'test-hync-8c8e13a6cd88.json', 'en-GB-Standard-F')
example.run()

