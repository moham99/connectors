from social_interaction_cloud.action import ActionRunner
from social_interaction_cloud.basic_connector import BasicSICConnector, RobotPosture
import time


class Demo:
    """For this example you will need to turn on the PeopleDetection and FaceRecognition services.
    When running this without a robot, you need the computer-camera, computer-robot and computer-speaker."""

    def __init__(self, server_ip: str):
        self.sic = BasicSICConnector(server_ip)
        self.action_runner = ActionRunner(self.sic)

        self.TRIGGER_DIST = 200  # in centimeters
        self.MIN_TIME_IN_FRAME = 5  # in seconds
        self.CLOSE_GREETING_TIMEOUT = 3  # in seconds

        self.in_close_greeting = False
        self.close_greeting_time = 0
        self.close_greeting_wait = 0

        self.WAIT = 5  # in seconds

        self.known_ids = set()
        self.ids_to_names = {0: "Thomas"}

    def run(self) -> None:
        self.sic.start()

        self.sic.set_use_stereo_camera(use_stereo_camera=True)
        self.action_runner.run_vision_listener('object', lambda a, b: a, continuous=True)
        self.action_runner.run_vision_listener('depth', lambda a, b: a, continuous=True)
        self.action_runner.run_vision_listener('tracking', self.raise_arm_if_close, True)
        self.action_runner.run_vision_listener('face', self.face_recognition, continuous=True)

        self.action_runner.run_action('set_eye_color', 'red')
        self.action_runner.run_action('start_looking', 0, 2)  # continuous mode requires ths explicitly

        time.sleep(100)
        self.stop()

    def raise_arm_if_close(self, object_id: int, distance_cm: int,
                           centroid_x: int, centroid_y: int,
                           in_frame_ms: int, est_speed_mps: int) -> None:
        print("Distance_cm:", distance_cm, 'in_frame_seconds:', in_frame_ms // 1000)

        # In different state
        if self.in_close_greeting:
            if self.close_greeting_time != 0 and (time.time() - self.close_greeting_time) > self.CLOSE_GREETING_TIMEOUT:
                print(f"Stopping close greeting... Close greeting time: {(time.time() - self.close_greeting_time)}")
                self.stop_close_greeting()
            return

        if distance_cm <= self.TRIGGER_DIST and in_frame_ms/1000 >= self.MIN_TIME_IN_FRAME:
            self.in_close_greeting = True
            # Greeting part
            print("Within trigger distance: setting eyes to green...")
            self.action_runner.run_action('set_eye_color', 'green')

            # Stop distance greeting and switch to close greeting on mono camera
            self.action_runner.run_action('stop_looking')
            time.sleep(0.25)
            self.sic.set_use_stereo_camera(use_stereo_camera=False)  # Switch to mono camera
            self.action_runner.run_action('start_looking', 0, 1)  # After camera switch this must be done explicitly
            time.sleep(0.25)
        else:
            self.in_close_greeting = False

    def face_recognition(self, identifier: str) -> None:
        # In a different state
        if not self.in_close_greeting or (time.time() - self.close_greeting_wait) < self.WAIT:
            return

        # Stop close greeting if longer than 2 seconds no person detected anymore
        if self.close_greeting_time != 0 and (time.time() - self.close_greeting_time) > self.CLOSE_GREETING_TIMEOUT:
            print(f"Stopping close greeting... Close greeting time: {(time.time() - self.close_greeting_time)}")
            self.stop_close_greeting()

        if self.close_greeting_time == 0:
            self.close_greeting_time = time.time()

        # No persons detected
        if identifier == '999':
            return

        known = identifier in self.known_ids
        self.known_ids.add(identifier)

        if known:
            print(f"Known person detected {identifier}: greeting...")
            self.action_runner.run_action('say', f'Hello, nice to see you again {self.ids_to_names[int(identifier)]}!')
        else:
            print(f"Unknown person detected {identifier}: added to known persons: greeting...")
            self.action_runner.run_action('say', f'Hello, nice to meet you.')

        self.close_greeting_wait = time.time()

        # Stop close greeting and switch back to distance greeting on stereo camera
        self.stop_close_greeting()

    def stop_close_greeting(self):
        self.close_greeting_time = 0
        self.action_runner.run_action('set_eye_color', 'red')

        self.action_runner.run_action('stop_looking')
        time.sleep(0.25)
        self.sic.set_use_stereo_camera(use_stereo_camera=True)  # Switch to stereo camera
        self.action_runner.run_action('start_looking', 0, 2)  # continuous mode requires ths explicitly
        time.sleep(0.25)
        self.in_close_greeting = False  # Stop face recognition

    def stop(self):
        self.action_runner.run_action('stop_looking')
        self.sic.stop()
        print("STOPPED")
        exit(0)


if __name__ == '__main__':
    example = Demo('sic.goalapl.dev')
    example.run()
