from social_interaction_cloud.action import ActionRunner
from social_interaction_cloud.basic_connector import BasicSICConnector
import time
import signal
from world import WorldInfo
from greeting_statemachine import StateMachine


class Demo:
    """For this example you will need to turn on the PeopleDetection and FaceRecognition services.
    When running this without a robot, you need the computer-camera, computer-robot and computer-speaker."""

    def __init__(self, server_ip: str):
        self.sic = BasicSICConnector(server_ip)
        self.action_runner = ActionRunner(self.sic)
        self.action_runner.run_action('stop_led_animation')  # Stop any led animation
        self.action_runner.run_action('set_idle')
        self.action_runner.run_action('set_breathing', True)
        time.sleep(2.0)
        self.action_runner.run_action('start_led_animation', 'eyes', 'blink', ['blue'], 7 * 1000, True)

        ids_to_name = {"0": "Thomas"}
        self.world_info = WorldInfo(self.action_runner, self.sic, ids_to_name=ids_to_name)
        self.statemachine = None

        self.data = []

    def run(self) -> None:
        self.sic.start()

        self.statemachine = StateMachine(self.world_info)

        self.sic.set_use_stereo_camera(use_stereo_camera=True)
        self.action_runner.run_vision_listener('object', lambda *args: None, continuous=True)
        self.action_runner.run_vision_listener('depth', lambda *args: None, continuous=True)
        self.action_runner.run_vision_listener('tracking', self.tracking_callback, True)
        self.action_runner.run_vision_listener('face', self.face_callback, continuous=True)
        time.sleep(2)
        self.action_runner.run_action('start_looking', 0, 2)  # continuous mode requires ths explicitly
        time.sleep(1)

    def tracking_callback(self, object_id: int, distance_cm: int,
                           centroid_x: int, centroid_y: int,
                           in_frame_ms: int, est_speed_mps: int) -> None:
        self.data.append((centroid_x, centroid_y, distance_cm))
        self.statemachine.on_event('tracked_person', locals())

    def face_callback(self, identifier: str) -> None:
        if identifier == '999':
            # 999 is a placeholder for no face detected
            self.statemachine.on_event('no_face_detected', dict())
        else:
            self.statemachine.on_event('detected_face', locals())

    def stop(self):
        self.action_runner.run_action('stop_looking')
        self.action_runner.run_action('stop_led_animation')
        self.sic.stop()

        self.plot_data()
        print("STOPPED")

    def plot_data(self):
        fx, fy = 309.90368652, 358.62393188
        cx, cy = 256.07534497, 219.93782487
        u_to_x = lambda u, Z: Z / fx * (u - cx)
        v_to_y = lambda v, Z: Z / fy * (v - cy)

        x = [u_to_x(u, z) for u, _, z in self.data]
        y = [v_to_y(v, z) for _, v, z in self.data]
        print(x, y)

        import matplotlib.pyplot as plt
        plt.scatter(x, y, label="Person")
        plt.scatter([0], [0], label="Pepper", color="red")
        plt.xlim([min(min(x), -10), max(max(x) * 1.1, 10)])
        plt.ylim([min(min(y), -10), max(max(y) * 1.1, 10)])
        plt.gca().set_aspect('equal', adjustable='box')
        plt.legend()
        plt.show()


if __name__ == '__main__':
    example = Demo('sic.goalapl.dev')

    def sigint_handler(signal, frame):
        print('Stopping...')
        example.stop()

    signal.signal(signal.SIGINT, sigint_handler)
    example.run()
