from greeting_statemachine import StateMachine
from world import WorldInfo


if __name__ == "__main__":
    robot = None
    sic = None
    world_info = WorldInfo(robot, sic, set(), {'0': "Thomas"})
    device = StateMachine(world_info)

    for i in range(10):
        device.on_event('tracked_person', {'distance_cm': 500, 'in_frame_ms': 1000 + i * 10})
    device.on_event('tracked_person', {'distance_cm': 400, 'in_frame_ms': 1100})
    device.on_event('tracked_person', {'distance_cm': 300, 'in_frame_ms': 1200})
    device.on_event('tracked_person', {'distance_cm': 300, 'in_frame_ms': 1300})
    device.on_event('tracked_person', {'distance_cm': 200, 'in_frame_ms': 1400})
    for i in range(20):
        device.on_event('tracked_person', {'distance_cm': 190, 'in_frame_ms': 1400 + i * 200})

    for i in range(1500):
        device.on_event('tracked_person', {'distance_cm': 190, 'in_frame_ms': 1400 + i * 200})

    device.on_event('detected_face', {'identifier': '0'})
    device.on_event('tracked_person', {'distance_cm': 190, 'in_frame_ms': 1400 + 1500 * 200})
    device.on_event('detected_face', {'identifier': '0'})
    device.on_event('tracked_person', {'distance_cm': 190, 'in_frame_ms': 1400 + 1501 * 200})

    device.on_event('detected_face', {'identifier': '1'})

    import time
    for i in range(50):
        time.sleep(0.5)
        device.on_event('tracked_person', {'distance_cm': 190, 'in_frame_ms': 301200 + i * 200})
        device.on_event('detected_face', {'identifier': '0'})
