from state import State
from world import WorldInfo
import time
import random

DISTANCE_GREETINGS = ["Hey!",
                      "Hello",
                      "Hello!",
                      "Hey"]

KNOWN_GREETINGS = ["Nice to see you again.",
                   "I recognize you!",
                   "I am happy to see you again."]

UNKOWN_GREETINGS = ["I am Herbert, nice to meet you.",
                    "Welcome to the Social AI lab!",
                    "I am Herbert, I hope you will enjoy it here."]

def check_mono_cam(sic, action_runner):
    if sic.use_stereo_camera:
        sic.set_use_stereo_camera(use_stereo_camera=False)
        action_runner.run_action('stop_looking')  # TODO waiting action or normal action?
        action_runner.run_action('start_looking', 0, 1)  # TODO waiting action or normal action?


def check_stereo_cam(sic, action_runner):
    if not sic.use_stereo_camera:
        sic.set_use_stereo_camera(use_stereo_camera=True)
        action_runner.run_action('stop_looking')  # TODO waiting action or normal action?
        action_runner.run_action('start_looking', 0, 2)  # TODO waiting action or normal action?


class Timeout:
    """
    A simple timeout class that returns True or False if the time is reached.
    """
    def __init__(self, total_time: float):
        self.total_time = total_time  # in seconds
        self.start_time = time.time()
        self.elapsed_time = 0.0  # in seconds

    def step(self) -> bool:
        self.elapsed_time = time.time() - self.start_time
        return self.check_done()

    def check_done(self):
        if self.elapsed_time >= self.total_time:
            print("Timed-out")
        return self.elapsed_time >= self.total_time


class ObservingState(State):
    """
    The basic state in which the robot is waiting to detect a person.
    """
    def __init__(self, world: WorldInfo):
        super().__init__()
        self.DISTANCE_TRIGGER = 250  # in cm
        self.TIME_TRIGGER = 0 * 1000  # in milliseconds
        self._world = world

        check_stereo_cam(self._world.get_sic(), self._world.get_robot())

    def on_event(self, event: str, kwargs: dict):
        if event == 'tracked_person':
            if kwargs.get('distance_cm') <= self.DISTANCE_TRIGGER and kwargs.get('in_frame_ms') >= self.TIME_TRIGGER:
                return ApproachingState(self._world)

        return self


class ApproachingState(State):
    """
    The state in which the robot has detected a person and is waiting to greet him/her.
    """
    def __init__(self, world: WorldInfo):
        super().__init__()
        self._world = world
        self.TIMEOUT = 8  # in seconds
        self.timer = Timeout(self.TIMEOUT)

        # Set robot to observing state: Hey gesture and eyes to green
        self._world.get_robot().run_action('say_animated', random.choice(DISTANCE_GREETINGS))
        check_mono_cam(self._world.get_sic(), self._world.get_robot())

    def on_event(self, event: str, kwargs: dict):
        if self.timer.step():
            return ObservingState(self._world)

        if event == 'detected_face':
            identifier = kwargs.get('identifier')
            if self._world.person_is_known(identifier):
                return KnownGreetingState(self._world, identifier)
            else:
                self._world.add_known_person(identifier)
                return NewPersonGreetingState(self._world)

        return self


class KnownGreetingState(State):
    """
    The state in which a known person is greeted with its name.
    """
    def __init__(self, world: WorldInfo, identifier: str) -> None:
        super().__init__()
        self._world = world
        self.identifier = identifier

        # Greet person
        # name = self._world.get_person_name(self.identifier)
        self._world.get_robot().run_action("say_animated", random.choice(KNOWN_GREETINGS))

    def on_event(self, event: str, kwargs: dict):
        return WaitAfterCloseGreetingState(self._world)


class NewPersonGreetingState(State):
    """
    The state in which a new (thus, unknown) person is greeted.
    """
    def __init__(self, world: WorldInfo) -> None:
        super().__init__()
        self._world = world

        # Greet person
        self._world.get_robot().run_action("say_animated", random.choice(UNKOWN_GREETINGS))

    def on_event(self, event: str, kwargs: dict):
        return WaitAfterCloseGreetingState(self._world)


class WaitAfterCloseGreetingState(State):
    """
    State in which we wait for TIMEOUT seconds before we continue to the next state.
    """
    def __init__(self, world: WorldInfo):
        super().__init__()
        self._world = world
        self.TIMEOUT = 8  # in seconds
        self.timer = Timeout(self.TIMEOUT)

    def on_event(self, event: str, kwargs: dict):
        if self.timer.step():
            return ObservingState(self._world)

        return self
