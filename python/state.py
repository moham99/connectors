from abc import ABC, abstractmethod


class State(ABC):
    """
    We define a state object which provides some utility functions for the
    individual states within the state machine.
    """

    def __init__(self):
        super().__init__()
        print('Processing current state:', self)

    @abstractmethod
    def on_event(self, event: str, kwargs: dict):
        """
        Handle events that are delegated to this State.
        """
        pass

    def __repr__(self) -> str:
        """
        Leverages the __str__ method to describe the State.
        """
        return self.__str__()

    def __str__(self) -> str:
        """
        Returns the name of the State.
        """
        return self.__class__.__name__
