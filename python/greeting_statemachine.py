from greeting_states import ObservingState
from world import WorldInfo


class StateMachine:
    """
    A simple state machine that mimics the behaviour of a robot on a high level.
    """

    def __init__(self, world_info: WorldInfo) -> None:
        """ Initialize the components. """

        # Start with a default state.
        self.state = ObservingState(world_info)

    def on_event(self, event, *args):
        """
        This is the bread and butter of the state machine. Incoming events are
        delegated to the given states which then handle the event. The result is
        then assigned as the new state.
        """
        # The next state will be the result of the on_event function.
        self.state = self.state.on_event(event, *args)
