package org.bitbucket.socialroboticshub.actions.audiovisual;

import java.util.List;

import org.bitbucket.socialroboticshub.actions.RobotAction;

import eis.iilang.Numeral;
import eis.iilang.Parameter;

public class StartWatchingAction extends RobotAction {
	public final static String NAME = "startWatching";

	/**
	 * @param parameters A list of 1 or 2 numerals, indicating the timeout (seconds)
	 *                   after which to stop watching and optionally the number of
	 *                   video channels desired (1 by default, 2 for stereo).
	 */
	public StartWatchingAction(final List<Parameter> parameters) {
		super(parameters);
	}

	@Override
	public boolean isValid() {
		final int params = getParameters().size();
		boolean valid = (params == 1 || params == 2);
		if (valid) {
			valid &= (getParameters().get(0) instanceof Numeral);
			if (params == 2) {
				valid &= (getParameters().get(1) instanceof Numeral);
			}
		}
		return valid;
	}

	@Override
	public String getTopic() {
		return "action_video";
	}

	@Override
	public String getData() {
		final String watchTime = EIStoString(getParameters().get(0));
		final String channelCount = (getParameters().size() == 1) ? "1" : EIStoString(getParameters().get(1));

		return (watchTime + ";" + channelCount);
	}

	@Override
	public String getExpectedEvent() {
		return "WatchingStarted";
	}
}
